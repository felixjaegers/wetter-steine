package ttn.adapter.mqtt;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import io.dropwizard.Application;
import io.dropwizard.lifecycle.Managed;
import io.dropwizard.setup.Environment;
import model.changes.ModelChanges;
import utils.service.dropwizard.DropwizardUtils;

public class Main {

    public static void main(String[] args) throws Exception {

        var application = new Application<ServiceConfiguration>() {

            @Override
            public void run(final ServiceConfiguration configuration, final Environment environment)
                    throws Exception {

                environment.getObjectMapper().registerModule(new JavaTimeModule())
                        .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
                    
                    DropwizardUtils.addCors(environment);

                var modelChangeRequestQueue = configuration.ProtocolAdapterRootQueue.create(
                        String.class, ModelChanges.class, environment.getObjectMapper());
                environment.lifecycle().manage(modelChangeRequestQueue);

                var mqttTtnSubscriberService = new MqttTtnSubscriberService(configuration,
                        modelChangeRequestQueue);

                var mqttTtnSubscriberServiceManager = new Managed(){
                
                    @Override
                    public void stop() throws Exception {
                        mqttTtnSubscriberService.stop();
                    }
                
                    @Override
                    public void start() throws Exception {
                        mqttTtnSubscriberService.start();                        
                    }
                };

                environment.lifecycle().manage(mqttTtnSubscriberServiceManager);
            }
        };

        application.run(args);
    }
}