package ttn.adapter.mqtt;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.function.BiConsumer;

import javax.naming.ConfigurationException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import model.changes.ModelChanges;

public class MqttTtnSubscriberService implements MqttCallback {

    private static final Logger logger = LoggerFactory.getLogger(MqttTtnSubscriberService.class);

    private static final UUID GATEWAY_ID = UUID.fromString("f4766c90-1566-428c-a3d6-cd2e9e3e5e40");

    private final MqttConfiguration mqttConfig;
    private final MqttClient mqttClient;
    private final MqttConnectOptions mqttConnOpts;

    private final BiConsumer<String, ModelChanges> modelChangesProducer;

    public MqttTtnSubscriberService(final ServiceConfiguration serviceConfiguration,
            final BiConsumer<String, ModelChanges> changesProducer) throws
            ConfigurationException, MqttException {

        this.mqttConfig = serviceConfiguration.MqttTtnSubscriberConfiguration;
        this.modelChangesProducer = changesProducer;

        this.mqttClient = new MqttClient(mqttConfig.getUri(), mqttConfig.getClientId());
        this.mqttConnOpts = new MqttConnectOptions();
    }

    public void start() throws MqttException, ConfigurationException {

        this.mqttConnOpts.setCleanSession(true);
        this.mqttConnOpts.setKeepAliveInterval(30);
        this.mqttConnOpts.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);
        this.mqttConnOpts.setAutomaticReconnect(true);

        if (mqttConfig.getCaCert() != null) {
            try {
                this.mqttConnOpts.setSocketFactory(getSocketFactory(this.mqttConfig.getCaCert()));
            } catch (Exception e) {
                logger.error(String.format("Couldn't create socket factory from file %s : %s",
                        this.mqttConfig.getCaCert(), e));
            }
        }

        if (mqttConfig.getUserName() != null) {
            if (mqttConfig.getPassword() == null) {
                throw new ConfigurationException("User name is set but password is not in the config.");
            }
            this.mqttConnOpts.setUserName(this.mqttConfig.getUserName());
            this.mqttConnOpts.setPassword(this.mqttConfig.getPassword().toCharArray());
        }

        try {
            this.mqttClient.setCallback(this);
            this.mqttClient.connect(this.mqttConnOpts);
            this.mqttClient.subscribe(this.mqttConfig.getTopic(), this.mqttConfig.getQos());
        } catch (MqttException me) {
            logger.error(String.format("MQTT exception - %s :" + System.lineSeparator() + "%s",
                    me.getMessage(), ExceptionUtils.getStackTrace(me)));
            this.stop();
            throw me;
        }
    }

    public void stop() {
        try {
            this.mqttClient.close();
        } catch (Exception e) {
            logger.warn("Couldn't close MQTT connection properly");
        }
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        logger.info(String.format("Got message %s from topic %s", message.toString(), topic));

        var modelChanges = new ModelChanges();
        modelChanges.TransactionId = UUID.randomUUID();
        var mapper = new ObjectMapper();

        var map = mapper.readValue(message.toString(), HashMap.class);

        TtnMessageParser ttnMessageParser;
        try {
            ttnMessageParser = new TtnMessageParser(map);
        } catch (Exception e) {
            logger.error(String.format("Skip message: %s", e));
            return;
        }

        var metadata = map.get("metadata");
        if (metadata instanceof HashMap<?, ?> == false) {
            throw new IllegalArgumentException("Object mapper read the wrond map type");
        }
        var dateTimeString = (String)((HashMap<?, ?>) metadata).get("time");

        modelChanges.ValuesSetOn = Instant.parse(dateTimeString).with(ChronoField.NANO_OF_SECOND, 0);
        modelChanges.ObjectChanges = ttnMessageParser.getValueChanges();
        logger.info(String.format("Parsed message: %s", mapper.writeValueAsString(modelChanges.ObjectChanges)));

        this.modelChangesProducer.accept(GATEWAY_ID.toString(), modelChanges);

        logger.info(String.format("Put parsed message to queue with transaction id '%s'", 
                modelChanges.TransactionId.toString()));
    }

    @Override
    public void connectionLost(Throwable cause) {
        logger.warn("Connection lost to MQTT Broker " + this.mqttConfig.toString(), cause);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        logger.warn("Delivery is not implemented");
    }

    private static SSLSocketFactory getSocketFactory(final String caCrtFile) throws IOException,
            CertificateException, KeyStoreException, NoSuchAlgorithmException,
            KeyManagementException {

        // load CA certificate
        List<X509Certificate> caCerts = new ArrayList<>();

        Path path = FileSystems.getDefault().getPath(caCrtFile);
        var bais = new ByteArrayInputStream(Files.readAllBytes(path));

        var fact = CertificateFactory.getInstance("X.509");
        try {
            X509Certificate x509Cert;
            while ((x509Cert = (X509Certificate) fact.generateCertificate(bais)) != null) {
                caCerts.add(x509Cert);
            }
        } catch (Exception e) {
        }

        // CA certificate is used to authenticate server
        KeyStore caKs = KeyStore.getInstance(KeyStore.getDefaultType());
        caKs.load(null, null);
        for (var caCert : caCerts) {
            caKs.setCertificateEntry("ca-certificate", caCert);
        }
        TrustManagerFactory tmf = TrustManagerFactory.getInstance("X509");
        tmf.init(caKs);

        // finally, create SSL socket factory
        SSLContext context = SSLContext.getInstance("TLSv1.2");
        context.init(null, tmf.getTrustManagers(), null);

        return context.getSocketFactory();
    }
}