export class IBaseObject {
    Id: string;
    TypeId: string;
    [memberId: string]: any;
}
