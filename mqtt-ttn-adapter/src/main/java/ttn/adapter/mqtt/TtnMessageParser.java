package ttn.adapter.mqtt;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import model.changes.ValueChange;
import model.changes.ValueChangeType;
import ttn.adapter.mqtt.objects.Location;
import ttn.adapter.mqtt.objects.WeatherStone;

public class TtnMessageParser {

    private Location location;

    private WeatherStone weatherStone;

    public TtnMessageParser(final Map<?, ?> message) throws IllegalArgumentException {

        this.location = new Location();
        this.weatherStone = new WeatherStone();

        this.parseMessage(message);
    }

    public Map<UUID, Map<String, ValueChange>> getValueChanges() {

        Map<String, ValueChange> location = new HashMap<>();

        location.put("TypeId", new ValueChange(Location.TypeId, ValueChangeType.Set));
        location.put("Name", new ValueChange(this.location.Name, ValueChangeType.Set));
        location.put("Latitude", new ValueChange(this.location.Latitude, ValueChangeType.Set));
        location.put("Longitude", new ValueChange(this.location.Longitude, ValueChangeType.Set));

        Map<String, ValueChange> stone = new HashMap<>();

        stone.put("TypeId", new ValueChange(WeatherStone.TypeId, ValueChangeType.Set));
        stone.put("Name", new ValueChange(this.weatherStone.Name, ValueChangeType.Set));
        stone.put("LocationId", new ValueChange(this.weatherStone.LocationId, ValueChangeType.Set));
        stone.put("Notifications", new ValueChange(this.weatherStone.Notifications,
                ValueChangeType.Set));

        Map<UUID, Map<String, ValueChange>> valueChanges = new HashMap<>();

        valueChanges.put(this.location.Id, location);
        valueChanges.put(this.weatherStone.Id, stone);

        return valueChanges;
    }

    private void parseMessage(final Map<?, ?> message) throws IllegalArgumentException {

        try {
            var dev_id = (String) message.get("dev_id");
            this.location.Id = UUID.nameUUIDFromBytes(("LocationId." + dev_id).getBytes());
            this.location.Name = "Location of " + dev_id;

            var payload = (String) message.get("payload_raw");
            var raw_bytes = new String(Base64.getDecoder().decode(payload));
            var parts = raw_bytes.split(";");

            var loc = parts[0].split(",");
            this.location.Latitude = Double.parseDouble(loc[0].replaceAll("[\\s+a-zA-Z :]", ""));
            this.location.Longitude = Double.parseDouble(loc[1].replaceAll("[\\s+a-zA-Z :]", ""));
    
            this.weatherStone.Id = UUID.nameUUIDFromBytes(dev_id.getBytes());
            this.weatherStone.Name = dev_id;
            this.weatherStone.LocationId = this.location.Id;

            this.weatherStone.Notifications = new HashMap<>();
            for (var part : parts) {
                if (part == parts[0]) {
                    continue;
                }

                part = part.replaceAll("[^a-zA-Z]", "");
                var values = new HashMap<String, Object>();
                values.put("Value", part);
                values.put("Active", true);
                switch (part) {
                    case "BL": this.weatherStone.Notifications.put("BatteryLow", values); break;
                    case "RAIN":
                    case "WIND":
                    case "SUN":
                        this.weatherStone.Notifications.put("MovementWeatherCode", values);
                        break;
                    default: break;
                }
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(
                    "Couldn't parse message: %s - because: %s", message, e));
        }
    }
}