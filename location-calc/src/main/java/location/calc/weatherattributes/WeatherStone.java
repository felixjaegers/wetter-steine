package location.calc.weatherattributes;

import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherStone {
    public UUID Id;
    public UUID LocationId;
    public Double GuessedLongitude;
    public Double GuessedLatitude;
    public String WeatherCode;
    public Map<String, Object> Notifications;
}