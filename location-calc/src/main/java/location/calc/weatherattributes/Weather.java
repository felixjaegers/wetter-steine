package location.calc.weatherattributes;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.List;

public class Weather {
    public List<Double> locatedAt;
    public ZonedDateTime validFrom;
    public ZonedDateTime validUntil;
    public Duration validPeriod;
}