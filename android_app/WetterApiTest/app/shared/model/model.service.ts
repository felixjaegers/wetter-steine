import { Injectable } from "@angular/core";
import { Headers, Http, Response } from "@angular/http";
import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";

import { Globals } from "./globals";
import { IBaseObject } from "./objects-model"

@Injectable()
export class ModelService {

    constructor(private httpClient: Http) { }

    getAllObjects(objectTypeId: string) {
        const url = Globals.apiUrl + "/v1/of-type/" + objectTypeId;

        return this.httpClient.get(url, {
            headers: this.getCommonHeaders()
        }).pipe(
            map((res) => res.json()),
            map((data) => {
                const modelObjects = [];
                for (const key of Object.keys(data)) {
                    modelObjects.push(data[key] as IBaseObject);
                }

                return modelObjects;
            }),
            catchError(this.handleErrors)
        );
    }

    getCommonHeaders() {
        const headers = new Headers();
        headers.append("Content-Type", "application/json");

        return headers;
    }

    handleErrors(error: Response) {
        console.log(JSON.stringify(error.json()));

        return Observable.throw(error);
    }

    getExistingObjectAttributeValue(modelObject: IBaseObject, path: string): any {
        if (modelObject == null) {
            return null;
        }

        return modelObject[path];
    }
}
