/*
 * Rui Santos 
 * Complete Project Details http://randomnerdtutorials.com
 */

#include <TinyGPS++.h>
#include <SoftwareSerial.h>

static const int RXPin = 7, TXPin = 6;
static const uint32_t GPSBaud = 9600;

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);

void setup(){
    Serial.begin(9600);
    ss.begin(GPSBaud);
    Serial.println("Starting with GPS");
}

void loop(){
    // This sketch displays information every time a new sentence is correctly encoded.
    while (ss.available() > 0){
        byte gpsdata = ss.read();
        Serial.write(gpsdata);
        gps.encode(gpsdata);
        if (gps.location.isUpdated()){
            Serial.println();
            Serial.print(gps.location.lat(), 6);
            Serial.print(","); 
            Serial.println(gps.location.lng(), 6);
        }
    }
}
