import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { TextField } from "tns-core-modules/ui/text-field";
import { ListViewEventData, RadListView } from "nativescript-ui-listview";
import { View } from "tns-core-modules/ui/core/view";

import { Grocery } from "../shared/grocery/grocery.model";
import { ModelService } from "../shared/model/model.service";
import { IBaseObject } from "../shared/model/objects-model";

@Component({
    selector: "gr-list",
    templateUrl: "list/list.component.html",
    styleUrls: ["list/list.component.css"],
    providers: [ModelService]
})
export class ListComponent implements OnInit {
    objectList: Array<IBaseObject> = [];
    grocery = "";
    isLoading = false;
    @ViewChild("groceryTextField") groceryTextField: ElementRef;

    constructor(private modelService: ModelService) { }

    ngOnInit() {
        this.isLoading = true;
        this.modelService.getAllObjects("14961228-07da-4351-b712-0809e33c01a9@0")
            .subscribe(loadedGroceries => {
                loadedGroceries.forEach((ob) => {
                    this.objectList.unshift(ob);
                });
                this.isLoading = false;
            });
    }
    }