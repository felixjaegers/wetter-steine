package location.calc;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.function.BiConsumer;

import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.zalando.stups.tokens.AccessTokens;
import org.zalando.stups.tokens.SimpleClientCredentials;
import org.zalando.stups.tokens.Tokens;

import io.dropwizard.lifecycle.Managed;
import model.changes.ModelChanges;

public class LocationCalculationService implements Managed {

    private final WeatherApiConfiguration weatherApiConfiguration;

    private final ModelRepositoryConfiguration modelRepositoryConfiguration;

    private final BiConsumer<String, ModelChanges> modelChangesProducer;

    private final Scheduler scheduler;

    private final AccessTokens accessTokens;

    public LocationCalculationService(
            final ServiceConfiguration serviceConfiguration,
            final BiConsumer<String, ModelChanges> changesProducer)
                throws URISyntaxException, SchedulerException {

        this.weatherApiConfiguration = serviceConfiguration.WeatherApiConfig;
        this.modelRepositoryConfiguration = serviceConfiguration.ModelRepositoryConfiguration;

        this.modelChangesProducer = changesProducer;
        this.scheduler = StdSchedulerFactory.getDefaultScheduler();

        this.accessTokens = Tokens.createAccessTokensWithUri(
                new URI(this.weatherApiConfiguration.getTokenUri()))
            .usingClientCredentialsProvider(
                () -> new SimpleClientCredentials("fj", this.weatherApiConfiguration.getClientId(),
                this.weatherApiConfiguration.getClientSecret()))
            .usingUserCredentialsProvider(() -> null)
            .manageToken("weather_api")
                .withGrantType("client_credentials")
                .done()
            .start();
    }

    public void start() throws SchedulerException, URISyntaxException {

        this.scheduler.start();

        var jobDataMap = new JobDataMap();
        jobDataMap.put("accessTokens", this.accessTokens);
        jobDataMap.put("modelChangesProducer", this.modelChangesProducer);
        jobDataMap.put("weatherApiRequest", this.getWeatherApiRequest());
        jobDataMap.put("modelRepositoryConfiguration", this.modelRepositoryConfiguration);

        var locationCalcJob = JobBuilder.newJob(LocationCalculationJob.class)
            .withIdentity("locationCalcJob")
            .setJobData(jobDataMap)
            .build();
        
        var locationCalcJobTrigger = TriggerBuilder.newTrigger()
            .withIdentity("locationCalcJobTrigger")
            .startNow()
            .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                .withIntervalInMinutes(this.weatherApiConfiguration.getRepeatMinutes())
                .repeatForever())
            .build();
        
        this.scheduler.scheduleJob(locationCalcJob, locationCalcJobTrigger);
    }

    public void stop() throws SchedulerException {

        this.scheduler.shutdown(true);
        this.accessTokens.stop();
    }

    private String getWeatherApiRequest() {
        var request = this.weatherApiConfiguration.getRequestUri();
        request += "fields=";
        var requestFields = this.weatherApiConfiguration.getRequestFields();
        for (var requestField : requestFields) {
            request += requestField;
            if (requestField != requestFields.get(requestFields.size() - 1)) {
                request += ",";
            }
        }
        request += "&validPeriod=";
        Duration longestDuration = Duration.of(0, ChronoUnit.HOURS);
        var requestPeriods = this.weatherApiConfiguration.getRequestPeriods();
        for (var requestPeriod : requestPeriods) {
            request += requestPeriod.toString();
            if (requestPeriod != requestPeriods.get(requestPeriods.size() - 1)) {
                request += ",";
            }
            if (0 > longestDuration.compareTo(requestPeriod)) {
                longestDuration = requestPeriod;
            }
        }
        request += "&locatedAt=13.2,52.4";

        var cal = Calendar.getInstance();
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        request += "&validFrom=" + cal.toInstant().toString();
        cal.add(Calendar.HOUR, longestDuration.toHoursPart());

        return  request + "&validUntil=" + cal.toInstant().toString();
    }
}