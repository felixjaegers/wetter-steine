package ttn.adapter.mqtt.objects;

import java.util.UUID;

public class Location {
    final public static String TypeId = "e86e0f1a-cc3a-4d7a-ab50-3c411d8da517@0";
    public UUID Id;
    public String Name;
    public Double Latitude;
    public Double Longitude;
}