package location.calc;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import io.dropwizard.Configuration;
import utils.service.kafka.KafkaProducerConfiguration;

public class ServiceConfiguration extends Configuration {

    @Valid
    @NotNull
    public KafkaProducerConfiguration ProtocolAdapterRootQueue = new KafkaProducerConfiguration();

    @Valid
    @NotNull
    public WeatherApiConfiguration WeatherApiConfig = new WeatherApiConfiguration();

    @Valid
    @NotNull
    public ModelRepositoryConfiguration ModelRepositoryConfiguration = new ModelRepositoryConfiguration();
}