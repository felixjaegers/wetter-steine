#define CFG_eu868 1
#define LMIC_DEBUG_LEVEL 0
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>

#define defaultsleep 100
#define shortsleep 100
#define TX_DEFAULT 60
#define TX_SHORT 2
#define SENDNR 25

int sleeptime = defaultsleep;


/*****************************************************************************
 *                                                                           *
                               GPS Pins Settings
 *                                                                           *
 *****************************************************************************/
static const int RXPin = 9, TXPin = 8; // rxpin is tx on gps, txpin is rx on gps
static const uint32_t GPSBaud = 9600;

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);


/*****************************************************************************
 *                                                                           *
                                 Lora Settings
 *                                                                           *
 *****************************************************************************/
// LoRaWAN NwkSKey, network session key
static const PROGMEM u1_t NWKSKEY[16] = { 0x47, 0x68, 0x76, 0x78, 0x37, 0x90, 0xC4, 0x8C, 0x82, 0x76, 0x8E, 0x6D, 0xCE, 0x36, 0x80, 0xD1 };

// LoRaWAN AppSKey, application session key
static const u1_t PROGMEM APPSKEY[16] = { 0xCA, 0x5F, 0xF2, 0x03, 0x1B, 0x07, 0x5A, 0x37, 0x43, 0x38, 0x9D, 0x9E, 0xDA, 0xE4, 0x22, 0x60 };

// LoRaWAN end-device address (DevAddr)
static const u4_t DEVADDR = 0x26011A1D ; // <-- Change this address for every node!

// These callbacks are only used in over-the-air activation, so they are
// left empty here (we cannot leave them out completely unless
// DISABLE_JOIN is set in config.h, otherwise the linker will complain).
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }

//static char mydata[40] = "00.000000,00.000000"; // payload for message
static char mydata[40] = "00.000000,00.000000"; // payload for message
static char statusdata[6] = {'\0'};
static int send_counter = 0;
static osjob_t sendjob;

// Schedule TX every this many seconds (might become longer due to duty
// cycle limitations).
unsigned TX_INTERVAL = TX_DEFAULT;

// Pin mapping
const lmic_pinmap lmic_pins = {
    .nss = 10,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 7,
    .dio = {4, 5, 6},
};


/*****************************************************************************
 *                                                                           *
                                 Accelerometer
 *                                                                           *
 *****************************************************************************/
//#define DEBUGSHAKE
//#define DEBUGCIRCLE
//#define DEBUGKNOCK
#define knockdiff 90
#define circdiff 15
const int xpin = A3; // x-axis
const int ypin = A2; // y-axis
const int zpin = A1; // z-axis
const int button = 2;
bool buttonActive = false;

struct AcceleratorValues {
    int x;
    int y;
    int z;
};

struct ShakeValues {
    byte shakeCount;
    char lastSign;
    int lastAxisdiff;
    unsigned long timestamp;
};

struct KnockValues {
    byte knockCount;
    unsigned int lastknock;
    unsigned long timestamp;
};

struct CircleValues {
    byte circleCount;
    int directionx;
    int directiony;
    int directionz;
    int lastx;
    int lasty;
    int lastz;
};

AcceleratorValues accel = {0, 0, 0};
ShakeValues shakevals;
KnockValues knockvals;
CircleValues circlevals;


/*****************************************************************************
 *                                                                           *
                                      Code
 *                                                                           *
 *****************************************************************************/

void onEvent (ev_t ev) {
    Serial.print(os_getTime());
    Serial.print(": ");
    switch(ev) {
        case EV_SCAN_TIMEOUT:
            Serial.println(F("EV_SCAN_TIMEOUT"));
            break;
        case EV_BEACON_FOUND:
            Serial.println(F("EV_BEACON_FOUND"));
            break;
        case EV_BEACON_MISSED:
            Serial.println(F("EV_BEACON_MISSED"));
            break;
        case EV_BEACON_TRACKED:
            Serial.println(F("EV_BEACON_TRACKED"));
            break;
        case EV_JOINING:
            Serial.println(F("EV_JOINING"));
            break;
        case EV_JOINED:
            Serial.println(F("EV_JOINED"));
            break;
        case EV_RFU1:
            Serial.println(F("EV_RFU1"));
            break;
        case EV_JOIN_FAILED:
            Serial.println(F("EV_JOIN_FAILED"));
            break;
        case EV_REJOIN_FAILED:
            Serial.println(F("EV_REJOIN_FAILED"));
            break;
        case EV_TXCOMPLETE:
            Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
            if (LMIC.txrxFlags & TXRX_ACK)
              Serial.println(F("Received ack"));
            if (LMIC.dataLen) {
              Serial.println(F("Received "));
              Serial.println(LMIC.dataLen);
              Serial.println(F(" bytes of payload"));
            }
            // Schedule next transmission
            os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
            break;
        case EV_LOST_TSYNC:
            Serial.println(F("EV_LOST_TSYNC"));
            break;
        case EV_RESET:
            Serial.println(F("EV_RESET"));
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            Serial.println(F("EV_RXCOMPLETE"));
            break;
        case EV_LINK_DEAD:
            Serial.println(F("EV_LINK_DEAD"));
            break;
        case EV_LINK_ALIVE:
            Serial.println(F("EV_LINK_ALIVE"));
            break;
         default:
            Serial.println(F("Unknown event"));
            break;
    }
}

void do_send(osjob_t* j){
    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND) {
        Serial.println(F("OP_TXRXPEND, not sending"));
    } else {
        // Prepare upstream data transmission at the next possible time.
        LMIC_setTxData2(1, mydata, sizeof(mydata)-1, 0);
        Serial.println(F("Packet queued"));
    }
    // Next TX is scheduled after TX_COMPLETE event.
}

void setup() {
    Serial.begin(9600);
    Serial.println(F("Starting"));

    strlcpy(statusdata, ";TEST", 6);

    // GPS
    ss.begin(GPSBaud);

    // Accelerometer
    pinMode(button, INPUT);
    attachInterrupt(digitalPinToInterrupt(button), ISR_Button, CHANGE);
    resetAll();

    #ifdef VCC_ENABLE
    // For Pinoccio Scout boards
    pinMode(VCC_ENABLE, OUTPUT);
    digitalWrite(VCC_ENABLE, HIGH);
    delay(1000);
    #endif

    // LMIC init
    os_init();
    // Reset the MAC state. Session and pending data transfers will be discarded.
    LMIC_reset();

    // Set static session parameters. Instead of dynamically establishing a session
    // by joining the network, precomputed session parameters are be provided.
    #ifdef PROGMEM
    // On AVR, these values are stored in flash and only copied to RAM
    // once. Copy them to a temporary buffer here, LMIC_setSession will
    // copy them into a buffer of its own again.
    uint8_t appskey[sizeof(APPSKEY)];
    uint8_t nwkskey[sizeof(NWKSKEY)];
    memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
    memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
    LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
    #else
    // If not running an AVR with PROGMEM, just use the arrays directly
    LMIC_setSession (0x1, DEVADDR, NWKSKEY, APPSKEY);
    #endif

    #if defined(CFG_eu868)
    // Set up the channels used by the Things Network, which corresponds
    // to the defaults of most gateways. Without this, only three base
    // channels from the LoRaWAN specification are used, which certainly
    // works, so it is good for debugging, but can overload those
    // frequencies, so be sure to configure the full frequency range of
    // your network here (unless your network autoconfigures them).
    // Setting up channels should happen after LMIC_setSession, as that
    // configures the minimal channel set.
    // NA-US channels 0-71 are configured automatically
    LMIC_setupChannel(0, 868100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(1, 868300000, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
    LMIC_setupChannel(2, 868500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(3, 867100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(4, 867300000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(5, 867500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(6, 867700000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(7, 867900000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(8, 868800000, DR_RANGE_MAP(DR_FSK,  DR_FSK),  BAND_MILLI);      // g2-band
    // TTN defines an additional channel at 869.525Mhz using SF9 for class B
    // devices' ping slots. LMIC does not have an easy way to define set this
    // frequency and support for class B is spotty and untested, so this
    // frequency is not configured here.
    #elif defined(CFG_us915)
    // NA-US channels 0-71 are configured automatically
    // but only one group of 8 should (a subband) should be active
    // TTN recommends the second sub band, 1 in a zero based count.
    // https://github.com/TheThingsNetwork/gateway-conf/blob/master/US-global_conf.json
    LMIC_selectSubBand(1);
    #endif

    // Disable link check validation
    LMIC_setLinkCheckMode(0);

    // TTN uses SF9 for its RX2 window.
    LMIC.dn2Dr = DR_SF9;

    // Set data rate and transmit power for uplink (note: txpow seems to be ignored by the library)
    LMIC_setDrTxpow(DR_SF7,14);

    // Start job
    //do_send(&sendjob);
}

int signum (int x) {
    return (x > 0) - (x < 0);
}

void resetShake() {
    shakevals = {0, +1, 0, millis()};
}

void resetKnock() {
    knockvals = {0, 0, 0};
}

void resetCircle() {
    circlevals = {0, 0, 0, 0, 0, 0, 0};
}

bool circleDiffsAboveThreshold(int xdiff, int ydiff, int zdiff) {
    int thresholdcount = 0; // at least two axis have to be above threshold
    if (abs(xdiff) > circdiff) thresholdcount++;
    if (abs(ydiff) > circdiff) thresholdcount++;
    if (abs(zdiff) > circdiff) thresholdcount++;
    return thresholdcount >= 2;
}

void checkCircle() {
    int xdiff = circlevals.lastx - accel.x;
    int ydiff = circlevals.lasty - accel.y;
    int zdiff = circlevals.lastz - accel.z;

    circlevals.directionx = signum(xdiff);
    circlevals.directiony = signum(ydiff);
    circlevals.directionz = signum(zdiff);

    if (circlevals.directionx == circlevals.directiony
            && circlevals.directionx != circlevals.directionz
            && circleDiffsAboveThreshold(xdiff, ydiff, zdiff)) {
        circlevals.circleCount++;
        #ifdef DEBUGCIRCLE
            Serial.println("Circle");
        #endif /* DEBUGCIRCLE */
    }

    circlevals.lastx = accel.x;
    circlevals.lasty = accel.y;
    circlevals.lastz = accel.z;

    if (circlevals.circleCount >= 5) {
        Serial.println("Woosh");
        strlcpy(statusdata, ";WIND", 6);
        setup_send_mode();
        resetCircle();
        resetAll();
    }

    #ifdef DEBUGCIRCLE
        Serial.print(xdiff);
        Serial.print(",");
        Serial.print(ydiff);
        Serial.print(",");
        Serial.print(zdiff);
        Serial.println();
        //Serial.print(circlevals.directionx);
        //Serial.print(",");
        //Serial.print(circlevals.directiony);
        //Serial.print(",");
        //Serial.print(circlevals.directionz);
        //Serial.println();
    #endif /* DEBUGCIRCLE */
}

void checkShake() {
    int axisdiff = accel.y - accel.x;
    char axisrelation = signum(axisdiff); // char for signed 8-bit val
    int cmpAxisdiff = axisdiff - shakevals.lastAxisdiff;

    #ifdef DEBUGSHAKE
        Serial.println(axisrelation, DEC);
        Serial.print(shakevals.shakeCount);
        Serial.print(" -- ");
        Serial.print(shakevals.lastSign, DEC);
        Serial.print(" -- ");
        Serial.print(abs(cmpAxisdiff), DEC);
        Serial.print(" -- ");
        Serial.println(millis() - shakevals.timestamp);
    #endif /* DEBUG */

    if (axisrelation != shakevals.lastSign
            && abs(cmpAxisdiff) > 100
            && (millis() - shakevals.timestamp) < 50000) {
        shakevals.shakeCount++;
        shakevals.lastSign *= -1;
    }

    if (shakevals.shakeCount >= 6) {
        Serial.println("Let the sunshine in");
        strlcpy(statusdata, ";SUN", 6);
        setup_send_mode();
        shakevals.shakeCount = 0;
        resetAll();
    }

    shakevals.lastAxisdiff = axisdiff;
}

void checkKnock() {
    int knockpower = accel.z - knockvals.lastknock;
    knockpower = abs(knockpower);

    #ifdef DEBUGKNOCK
        Serial.print(accel.z);
        Serial.print(" -- ");
        Serial.print(knockvals.lastknock);
        Serial.print(" -- ");
        Serial.print(knockpower);
        Serial.println();
    #endif /* DEBUG */
    if (accel.z - knockvals.lastknock > knockdiff
            && (millis() - knockvals.timestamp) > 250) {
        knockvals.lastknock = accel.z;
        knockvals.timestamp = millis();
        knockvals.knockCount++;
    }

    if (knockvals.knockCount > 5) {
        Serial.println("Make it Rain");
        strlcpy(statusdata, ";RAIN", 6);
        setup_send_mode();
        knockvals.knockCount = 0;
        resetAll();
    }
}


void checkMotion() {
    checkKnock();
    checkShake();
    checkCircle();
}

void resetAll() {
    resetShake();
    resetKnock();
    resetCircle();
}

void ISR_Button() {
    // reset values
    resetAll();

    // set buttonstate
    if (digitalRead(button) == HIGH) {
        sleeptime = shortsleep;
        buttonActive = true;
    } else {
        sleeptime = defaultsleep;
        buttonActive = false;
    }
}

void check_send_mode() {
    strlcpy(&mydata[19], statusdata, 6);
    if (send_counter > 0) {
        if (! (LMIC.opmode & OP_TXRXPEND)) { // send only if no send queued
            Serial.println("---- Will send");
            send_counter--;
            do_send(&sendjob);
        } else {
            Serial.println("---- Wont send");
        }
    } else {
        TX_INTERVAL = TX_DEFAULT;
        memset(statusdata, '\0', 6);
    }
}

void setup_send_mode() {
    send_counter = SENDNR;
    TX_INTERVAL = TX_SHORT;
}

void printData() {
    Serial.print(accel.x);
    Serial.print(",");
    Serial.print(accel.y);
    Serial.print(",");
    Serial.println(accel.z);
}

void loop() {
    accel.x = analogRead(xpin);
    accel.y = analogRead(ypin);
    accel.z = analogRead(zpin);

    if (buttonActive) {
        checkMotion();
    }

    //printData();

    while (ss.available() > 0){
        byte msg = ss.read();
        //Serial.write(msg);
        gps.encode(msg);
        if (gps.location.isUpdated()){
            dtostrf(gps.location.lat(), 9, 6, mydata);
            mydata[9] = ',';
            dtostrf(gps.location.lng(), 9, 6, &mydata[10]);
            mydata[19] = '\0';
            //Serial.print(gps.location.lat(), 6);
            //Serial.print(",");
            //Serial.println(gps.location.lng(), 6);
            Serial.println();
            //Serial.println(mydata);
        }
    }

    //Serial.println();
    Serial.println(TX_INTERVAL);
    Serial.println(send_counter);

    check_send_mode();
    Serial.println(statusdata);
    Serial.println(mydata);
    os_runloop_once();


    delay(sleeptime);
}
