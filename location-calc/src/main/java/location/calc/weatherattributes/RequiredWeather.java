package location.calc.weatherattributes;

public class RequiredWeather extends Weather {
    public Double averageWindSpeedInBeaufort;
    public Double globalRadiationInJoulePerSquareCentimeter;
    public Double precipitationAmountInMillimeter;
    public Double precipitationProbabilityInPercent;
}