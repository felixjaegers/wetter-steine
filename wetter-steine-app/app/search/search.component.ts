import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { ModelService } from "~/model/model.service";
import { IBaseObject } from "~/model/objects-model";

@Component({
    selector: "Search",
    moduleId: module.id,
    templateUrl: "./search.component.html",
    providers: [ModelService]
})
export class SearchComponent implements OnInit {

    wettersteine: ObservableArray<IBaseObject> = new ObservableArray<IBaseObject>();
    groceryList: ObservableArray<IBaseObject> = null;

    constructor(private modelService: ModelService) { }

    get getGroceryList(): ObservableArray<IBaseObject> {
        return new ObservableArray<IBaseObject>([{
            Id: "c1fff137-699e-4751-9f43-fa910c5936e6",
            TypeId: "14961228-07da-4351-b712-0809e33c01a9@0",
            Name: "Apples"
        }, {
            Id: "b516f3d4-4076-4cb8-b84e-224c4f80304b",
            TypeId: "14961228-07da-4351-b712-0809e33c01a9@0",
            Name: "Birne"
        }
    ]);
    }

    ngOnInit(): void {
        const fooArr = new Array(["foo", "bar", "baz"]);
        this.groceryList = new ObservableArray<IBaseObject>([{
                Id: "c1fff137-699e-4751-9f43-fa910c5936e6",
                TypeId: "14961228-07da-4351-b712-0809e33c01a9@0",
                Name: "Apples"
            }, {
                Id: "b516f3d4-4076-4cb8-b84e-224c4f80304b",
                TypeId: "14961228-07da-4351-b712-0809e33c01a9@0",
                Name: "Birne"
            }
        ]);
        console.log(fooArr);
        console.log(this.getGroceryList);
        // this.groceryList.push({
        //     Id: "c1fff137-699e-4751-9f43-fa910c5936e6",
        //     TypeId: "14961228-07da-4351-b712-0809e33c01a9@0",
        //     Name: "Apples"
        // });
        // this.groceryList.push({
        //     Id: "b516f3d4-4076-4cb8-b84e-224c4f80304b",
        //     TypeId: "14961228-07da-4351-b712-0809e33c01a9@0",
        //     Name: "Birne"
        // });

        // this.modelService.getAllObjects("14961228-07da-4351-b712-0809e33c01a9@0")
        //     .subscribe((loadedModel) => {
        //         loadedModel.forEach((ob) => {
        //             this.wettersteine.unshift(ob);
        //         });
        //     });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
