package location.calc;

import java.time.Duration;
import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.validator.constraints.NotEmpty;

public class WeatherApiConfiguration {

    @NotEmpty
    private String tokenUri;

    /**
     * @return the uri
     */
    public String getTokenUri() {
        return tokenUri;
    }

    @NotEmpty
    private String clientId;

    /**
     * @return the clientId
     */
    public String getClientId() {
        return clientId;
    }

    @NotEmpty
    private String clientSecret;

    /**
     * @return the clientSecret
     */
    public String getClientSecret() {
        return clientSecret;
    }

    @NotEmpty
    private String requestUri;

    /**
     * @return the requestUri
     */
    public String getRequestUri() {
        return requestUri;
    }

    @NotEmpty
    private List<String> requestFields;

    /**
     * @return the requestFields
     */
    public List<String> getRequestFields() {
        return requestFields;
    }

    @NotEmpty
    private List<Duration> requestPeriods;

    /**
     * @return the requestPeriods
     */
    public List<Duration> getRequestPeriods() {
        return requestPeriods;
    }

    @Min(1)
    @Max(1440)
    @JsonProperty
    private int repeatMinutes = 1;

    /**
     * @return the repeatMinutes
     */
    @JsonProperty
    public int getRepeatMinutes() {
        return repeatMinutes;
    }
}