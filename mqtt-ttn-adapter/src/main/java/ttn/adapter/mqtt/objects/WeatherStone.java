package ttn.adapter.mqtt.objects;

import java.util.Map;
import java.util.UUID;

public class WeatherStone {
    final public static String TypeId = "14961228-07da-4351-b712-0809e33c01a9@0";
    public UUID Id;
    public String Name;
    public UUID LocationId;
    public Map<String, Map<String, Object>> Notifications;
}