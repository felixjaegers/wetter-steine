package location.calc;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoField;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.function.BiConsumer;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zalando.stups.tokens.AccessTokens;

import location.calc.weatherattributes.ForecastWeather;
import location.calc.weatherattributes.StoneLocation;
import location.calc.weatherattributes.WeatherStone;
import model.changes.ModelChanges;
import model.changes.ValueChange;
import model.changes.ValueChangeType;

public class LocationCalculationJob implements Job {

    private static final Logger logger = LoggerFactory.getLogger(LocationCalculationJob.class);

    private AccessTokens accessTokens;

    private static final UUID GATEWAY_ID = UUID.fromString("6ff848c7-c15a-49dc-a3c9-7806195151f4");

    /**
     * @param accessTokens the accessTokens to set
     */
    public void setAccessTokens(AccessTokens accessTokens) {
        this.accessTokens = accessTokens;
    }

    private BiConsumer<String, ModelChanges> modelChangesProducer;

    /**
     * @param modelChangesProducer the modelChangesProducer to set
     */
    public void setModelChangesProducer(BiConsumer<String, ModelChanges> modelChangesProducer) {
        this.modelChangesProducer = modelChangesProducer;
    }

    private String weatherApiRequest;

    /**
     * @param weatherApiRequest the request to set
     */
    public void setWeatherApiRequest(String weatherApiRequest) {
        this.weatherApiRequest = weatherApiRequest;
    }

    private ModelRepositoryConfiguration modelRepositoryConfiguration;

    /**
     * @param modelRepositoryConfiguration the modelRepositoryConfiguration to set
     */
    public void setModelRepositoryConfiguration(ModelRepositoryConfiguration modelRepositoryConfiguration) {
        this.modelRepositoryConfiguration = modelRepositoryConfiguration;
    }

    public LocationCalculationJob() {
    }

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
        var httpClient = HttpClients.createDefault();
        var mapper = new ObjectMapper();
        var modelChanges = new ModelChanges();

        modelChanges.TransactionId = UUID.randomUUID();
        modelChanges.ObjectChanges = new HashMap<UUID, Map<String, ValueChange>>();
        mapper.registerModule(new JavaTimeModule());

        try {
            var weatherStones = this.getWeatherStones(httpClient, mapper);
            var stoneLocation = this.getStoneLocation(httpClient, mapper, weatherStones);
            var weather = this.getWeather(httpClient, mapper);
            
            modelChanges.ValuesSetOn = Instant.now().with(ChronoField.NANO_OF_SECOND, 0);
            for (var weatherStone : weatherStones.values()) {
                modelChanges.ObjectChanges.put(weatherStone.Id,
                        this.getGuessedStone(weatherStone, weather, stoneLocation));
            }

            logger.info(String.format("Parsed message: %s",
                    mapper.writeValueAsString(modelChanges.ObjectChanges)));

            this.modelChangesProducer.accept(GATEWAY_ID.toString(), modelChanges);
    
            logger.info(String.format("Put parsed message to queue with transaction id '%s'", 
                    mapper.writeValueAsString(modelChanges.TransactionId)));
        } catch (IOException e) {
			e.printStackTrace();
		}
    }

    private Map<String, ValueChange> getGuessedStone(final WeatherStone weatherStone,
            final ForecastWeather weather, final Map<UUID, StoneLocation> locations) {
        
        for (var weatherPoint : weather.forecasts) {
            switch (weatherStone.WeatherCode) {
                case "SUN":
                    if (weatherPoint.globalRadiationInJoulePerSquareCentimeter == null) {
                        continue;
                    } else {
                        var precision = weatherPoint.globalRadiationInJoulePerSquareCentimeter / 20;
                        return this.getGuessedLocation(precision, locations.get(weatherStone.LocationId));
                    }
                case "WIND":
                    if (weatherPoint.averageWindSpeedInBeaufort == null) {
                        continue;
                    } else {
                        var precision = weatherPoint.averageWindSpeedInBeaufort * 1.3;
                        return this.getGuessedLocation(precision, locations.get(weatherStone.LocationId));
                    }
                case "RAIN":
                    if (weatherPoint.precipitationProbabilityInPercent == null) {
                        continue;
                    } else {
                        var precision = weatherPoint.precipitationProbabilityInPercent / 10;
                        return this.getGuessedLocation(precision, locations.get(weatherStone.LocationId));
                    }
                default:
                    break;
            }
        }
        throw new NotImplementedException("The weather code " +
                weatherStone.WeatherCode + " is not implemented.");
    }

    private Map<String, ValueChange> getGuessedLocation(final Double precision,
            final StoneLocation location) {

        Map<String, ValueChange> guessedCoords = new HashMap<>();
        var rand = new Random();

        var lonValueChange = new ValueChange(location.Longitude +
                rand.nextDouble() * Math.pow(10, -precision), ValueChangeType.Set);
        var latValueChange = new ValueChange(location.Latitude +
                rand.nextDouble() * Math.pow(10, -precision), ValueChangeType.Set);
        guessedCoords.put("GuessedLongitude", lonValueChange);
        guessedCoords.put("GuessedLatitude", latValueChange);

        return guessedCoords;
    }

    private Map<UUID, StoneLocation> getStoneLocation(final CloseableHttpClient httpClient,
            final ObjectMapper mapper, final Map<UUID, WeatherStone> weatherStones)
            throws IOException {

        var stoneLocations = new HashMap<UUID, StoneLocation>();
        try {
            for (var weatherStone : weatherStones.entrySet()) {
                var modelTypeRequest = "http://" + this.modelRepositoryConfiguration.getHost() +
                        ":" + this.modelRepositoryConfiguration.getPort() + "/v1/" +
                        weatherStone.getValue().LocationId;
                var httpGet = new HttpGet(modelTypeRequest);

                var responseEntity = httpClient.execute(httpGet).getEntity();
                var jsonContent = EntityUtils.toString(responseEntity);
                logger.debug(jsonContent);

                if (responseEntity.getContentType().getValue().contains("application/json")) {
                    var location = mapper.readValue(jsonContent, StoneLocation.class);
                    stoneLocations.put(location.Id, location);
                } else {
                    throw new IOException("Wrong content type");
                }
            }

            return stoneLocations;
        } catch (IOException e) {
            logger.error("HTTP request error: ", e);
            throw e;
        }
    }

    private Map<UUID, WeatherStone> getWeatherStones(final CloseableHttpClient httpClient,
            final ObjectMapper mapper) throws IOException {
        var modelTypeRequest = "http://" + this.modelRepositoryConfiguration.getHost() +
                ":" + this.modelRepositoryConfiguration.getPort() + "/v1/of-type/" +
                this.modelRepositoryConfiguration.getWeatherStoneTypeId();
        var httpGet = new HttpGet(modelTypeRequest);

        try {
            var responseEntity = httpClient.execute(httpGet).getEntity();
            var jsonContent = EntityUtils.toString(responseEntity);
            logger.debug(jsonContent);

            if (responseEntity.getContentType().getValue().contains("application/json")) {
                Map<UUID, WeatherStone> weatherStones = new HashMap<>();
                TypeReference<HashMap<UUID, WeatherStone>> typeRef =
                        new TypeReference<HashMap<UUID, WeatherStone>>() {};
                weatherStones = mapper.readValue(jsonContent, typeRef);

                return weatherStones;
            } else {
                throw new IOException("Wrong content type");
            }
		} catch (IOException e) {
            logger.error("HTTP request error: " + httpGet.toString(), e);
            throw e;
        }
	}

	private ForecastWeather getWeather(final CloseableHttpClient httpClient,
            final ObjectMapper mapper) throws IOException {
        var accessToken = this.accessTokens.get("weather_api");

        var httpGetWeather = new HttpGet(this.weatherApiRequest);
        httpGetWeather.setHeader("Authorization", "Bearer " + accessToken);
        logger.debug("Send HTTP weatherApiRequest " + httpGetWeather.toString());

        try {
            var responseEntity = httpClient.execute(httpGetWeather).getEntity();
            var jsonContent = EntityUtils.toString(responseEntity);
            logger.debug(jsonContent);

            if (responseEntity.getContentType().getValue().contains("application/json") &&
                    responseEntity.getContentType().getValue().contains("charset=UTF-8")) {
                mapper.registerModule(new JavaTimeModule());
                var obj = mapper.readValue(jsonContent, ForecastWeather.class);

                return obj;
            } else {
                throw new IOException("Wrong content type or charset");
            }
		} catch (IOException e) {
            logger.error("HTTP request error: " + httpGetWeather.toString(), e);
            throw e;
        }
    }
}