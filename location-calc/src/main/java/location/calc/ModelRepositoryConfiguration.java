package location.calc;

import org.hibernate.validator.constraints.NotEmpty;

public class ModelRepositoryConfiguration {

    @NotEmpty
    private String host;

    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

    @NotEmpty
    private String port;

    /**
     * @return the port
     */
    public String getPort() {
        return port;
    }

    @NotEmpty
    private String weatherStoneTypeId;

    /**
     * @return the weatherStoneTypeId
     */
    public String getWeatherStoneTypeId() {
        return weatherStoneTypeId;
    }
}