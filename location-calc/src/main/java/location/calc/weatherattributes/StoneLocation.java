package location.calc.weatherattributes;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StoneLocation {
    public UUID Id;
    public Double Longitude;
    public Double Latitude;
}