package ttn.adapter.mqtt;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MqttConfiguration {

    @Min(1)
    @Max(65535)
    @JsonProperty
    private int port = 8883;

    @NotEmpty
    private String hostName;

    /**
     * @return the hostName
     */
    @JsonProperty
    public String getUri() {
        return String.format("%s://%s:%d", this.protocol, this.hostName, this.port);
    }

    /**
     * @param hostName the hostName to set
     */
    @JsonProperty
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    @NotEmpty
    private String topic;

    @JsonProperty
    public String getTopic() {
        return this.topic;
    }

    @JsonProperty
    public void setTopic(String topic) {
        this.topic = topic;
    }

    private String clientId = "28d7124c-ab07-4bbd-9a6f-b1ee6976d62f";

    /**
     * @return the clientId
     */
    public String getClientId() {
        return clientId;
    }

    private int qos = 2;

    /**
     * @return the qos
     */
    public int getQos() {
        return qos;
    }

    private String caCert;

    /**
     * @return the caCert
     */
    public String getCaCert() {
        return caCert;
    }

    @JsonProperty
    private String protocol = "tcp";

    private String userName;

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    private String password;

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }
}