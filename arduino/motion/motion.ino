//#define DEBUGSHAKE
//#define DEBUGCIRCLE
//#define DEBUGKNOCK
#define defaultsleep 1000
#define shortsleep 100
#define knockdiff 90
#define circdiff 15
const int xpin = A3; // x-axis
const int ypin = A2; // y-axis
const int zpin = A1; // z-axis
const int button = 2;
bool buttonActive = false;
int sleeptime = defaultsleep;

struct AcceleratorValues {
    int x;
    int y;
    int z;
};

struct ShakeValues {
    byte shakeCount;
    char lastSign;
    int lastAxisdiff;
    unsigned long timestamp;
};

struct KnockValues {
    byte knockCount;
    unsigned int lastknock;
    unsigned long timestamp;
};

struct CircleValues {
    byte circleCount;
    int directionx;
    int directiony;
    int directionz;
    int lastx;
    int lasty;
    int lastz;
};

AcceleratorValues accel = {0, 0, 0};
ShakeValues shakevals;
KnockValues knockvals;
CircleValues circlevals;

int signum (int x) {
    return (x > 0) - (x < 0);
}

void resetShake() {
    shakevals = {0, +1, 0, millis()};
}

void resetKnock() {
    knockvals = {0, 0, 0};
}

void resetCircle() {
    circlevals = {0, 0, 0, 0, 0, 0, 0};
}

bool circleDiffsAboveThreshold(int xdiff, int ydiff, int zdiff) {
    int thresholdcount = 0; // at least two axis have to be above threshold
    if (abs(xdiff) > circdiff) thresholdcount++;
    if (abs(ydiff) > circdiff) thresholdcount++;
    if (abs(zdiff) > circdiff) thresholdcount++;
    return thresholdcount >= 2;
}

void checkCircle() {
    int xdiff = circlevals.lastx - accel.x;
    int ydiff = circlevals.lasty - accel.y;
    int zdiff = circlevals.lastz - accel.z;

    circlevals.directionx = signum(xdiff);
    circlevals.directiony = signum(ydiff);
    circlevals.directionz = signum(zdiff);

    if (circlevals.directionx == circlevals.directiony
            && circlevals.directionx != circlevals.directionz
            && circleDiffsAboveThreshold(xdiff, ydiff, zdiff)) {
        circlevals.circleCount++;
        //Serial.println("Circle");
    }

    circlevals.lastx = accel.x;
    circlevals.lasty = accel.y;
    circlevals.lastz = accel.z;

    if (circlevals.circleCount >= 5) {
        Serial.println("Woosh");
        resetCircle();
        resetAll();
    }

    #ifdef DEBUGCIRCLE
        Serial.print(xdiff);
        Serial.print(",");
        Serial.print(ydiff);
        Serial.print(",");
        Serial.print(zdiff);
        Serial.println();
        //Serial.print(circlevals.directionx);
        //Serial.print(",");
        //Serial.print(circlevals.directiony);
        //Serial.print(",");
        //Serial.print(circlevals.directionz);
        //Serial.println();
    #endif /* DEBUGCIRCLE */
}

void checkShake() {
    int axisdiff = accel.y - accel.x;
    char axisrelation = signum(axisdiff); // char for signed 8-bit val
    int cmpAxisdiff = axisdiff - shakevals.lastAxisdiff;

    #ifdef DEBUGSHAKE
        Serial.println(axisrelation, DEC);
        Serial.print(shakevals.shakeCount);
        Serial.print(" -- ");
        Serial.print(shakevals.lastSign, DEC);
        Serial.print(" -- ");
        Serial.print(abs(cmpAxisdiff), DEC);
        Serial.print(" -- ");
        Serial.println(millis() - shakevals.timestamp);
    #endif /* DEBUG */

    if (axisrelation != shakevals.lastSign
            && abs(cmpAxisdiff) > 100
            && (millis() - shakevals.timestamp) < 50000) {
        shakevals.shakeCount++;
        shakevals.lastSign *= -1;
    }

    if (shakevals.shakeCount >= 6) {
        Serial.println("Let the sunshine in");
        shakevals.shakeCount = 0;
        resetAll();
    }

    shakevals.lastAxisdiff = axisdiff;
}

void checkKnock() {
    int knockpower = accel.z - knockvals.lastknock;
    knockpower = abs(knockpower);

    #ifdef DEBUGKNOCK
        Serial.print(accel.z);
        Serial.print(" -- ");
        Serial.print(knockvals.lastknock);
        Serial.print(" -- ");
        Serial.print(knockpower);
        Serial.println();
    #endif /* DEBUG */
    if (accel.z - knockvals.lastknock > knockdiff
            && (millis() - knockvals.timestamp) > 250) {
        knockvals.lastknock = accel.z;
        knockvals.timestamp = millis();
        knockvals.knockCount++;
    }

    if (knockvals.knockCount > 5) {
        Serial.println("Make it Rain");
        knockvals.knockCount = 0;
        resetAll();
    }
}


void checkMotion() {
    checkKnock();
    checkShake();
    checkCircle();
}

void resetAll() {
    resetShake();
    resetKnock();
    resetCircle();
}

void ISR_Button() {
    // reset values
    resetAll();

    // set buttonstate
    if (digitalRead(button) == HIGH) {
        sleeptime = shortsleep;
        buttonActive = true;
    } else {
        sleeptime = defaultsleep;
        buttonActive = false;
    }
}

void printData() {
    Serial.print(accel.x);
    Serial.print(",");
    Serial.print(accel.y);
    Serial.print(",");
    Serial.println(accel.z);
}

void setup() {
    Serial.begin(9600);
    pinMode(button, INPUT);
    attachInterrupt(digitalPinToInterrupt(button), ISR_Button, CHANGE);
    resetShake();
    resetKnock();
    resetCircle();
}

void loop() {
    accel.x = analogRead(xpin);
    accel.y = analogRead(ypin);
    accel.z = analogRead(zpin);

    if (buttonActive) {
        checkMotion();
    }

    //printData();


    delay(sleeptime);
}
