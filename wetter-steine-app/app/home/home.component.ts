import { Component, OnInit, ViewChild } from "@angular/core";
import * as app from "application";
import { registerElement } from "nativescript-angular/element-registry";
import * as geolocation from "nativescript-geolocation";
import { MapView, Marker, Position } from "nativescript-google-maps-sdk";
import { SnackBar, SnackBarOptions } from "nativescript-snackbar";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { clearInterval, setInterval } from "timer";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
// import { Timer } from "tns-core-modules/timer";
import { Accuracy } from "ui/enums";
import { ModelService } from "~/model/model.service";
import { IBaseObject } from "~/model/objects-model";

registerElement("MapView", () => MapView);

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    providers: [ModelService]
})
export class HomeComponent implements OnInit {
    wettersteine: Array<IBaseObject> = [];
    snackbar: SnackBar;

    latitude =  52.545123;
    longitude = 13.351367;
    zoom = 8;
    minZoom = 0;
    maxZoom = 22;
    bearing = 0;
    tilt = 0;
    padding = [40, 40, 40, 40];
    mapView: MapView;

    setInterval = setInterval;
    stoneMarked = "";
    markSet = false;


    lastCamera: string;

    constructor(private modelService: ModelService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.snackbar = new SnackBar();
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

   // Map events
    onMapReady(event) {
        console.log("Map Ready");

        geolocation.enableLocationRequest(); // Request permission
        this.mapView = event.object;
        this.mapView.settings.myLocationButtonEnabled = true;

        console.log("Setting Location");
        geolocation.getCurrentLocation({
            desiredAccuracy: Accuracy.high,
            maximumAge: 5000,
            timeout: 20000
        }).then((pos) => {
            console.log("Got lat: ", pos.latitude);
            console.log("Got lon: ", pos.longitude);
            this.mapView.latitude = pos.latitude;
            this.mapView.longitude = pos.longitude;

        });

        this.gatherWeatherStones();
        this.setInterval(() => {
            this.checkFoundStone();
        }, 1000);
    }

    onCoordinateTapped(args) {
        this.markSet = false;
        this.stoneMarked = "";

        console.log("Coordinate Tapped, Lat: " + args.position.latitude + ", Lon: " + args.position.longitude);
    }

    onMarkerEvent(args) {
        this.markSet = true;
        this.stoneMarked = args.marker.title;


        console.log("Marker Event: '" + args.eventName
            + "' triggered on: " + args.marker.title
            + ", Lat: " + args.marker.position.latitude + ", Lon: " + args.marker.position.longitude);
    }

    onCameraChanged(args) {
        console.log("Camera changed: " + JSON.stringify(args.camera), JSON.stringify(args.camera) === this.lastCamera);
        console.log("Wettersteine Attribute: " + JSON.stringify(this.wettersteine));
        this.lastCamera = JSON.stringify(args.camera);
    }

    gatherWeatherStones() {
        console.log("Gathering Weather stones");
        if (! this.markSet) { // don't overwrite selection
            this.mapView.removeAllMarkers(); // clean markers
        }

        this.modelService.getAllObjects("14961228-07da-4351-b712-0809e33c01a9@0")
            .subscribe((loadedModel) => {
                loadedModel.forEach((ob) => {
                    this.wettersteine.unshift(ob);
                    if (! this.markSet) { // don't overwrite selection
                        this.markWeatherStone(ob);
                    }
            });
        });
    }

    markWeatherStone(wetterstein: IBaseObject) {
        if (wetterstein.GuessedLatitude && wetterstein.GuessedLongitude) {
            const marker = new Marker();
            marker.position = Position.positionFromLatLng(wetterstein.GuessedLatitude, wetterstein.GuessedLongitude);
            marker.title = wetterstein.Name;

            if (wetterstein.WeatherCode === "WIND") {
                marker.snippet = "Ein Windstein";
            } else if (wetterstein.WeatherCode === "SUN") {
                marker.snippet = "Ein Sonnenstein!";
            } else if (wetterstein.WeatherCode === "RAIN") {
                marker.snippet = "Ein Regenstein!";
            } else {
                marker.snippet = wetterstein.WeatherCode;
            }

            marker.userData = {index: 1};
            this.mapView.addMarker(marker);
        }
    }

    checkFoundStone() {
        if (! this.markSet) {
            return; // dont check if no stone found
        }
        console.log("mark ist set");
        this.gatherWeatherStones();

        for (let i = 0, len = this.wettersteine.length; i < len; i++) {
            const stone = this.wettersteine[i];
            console.log("stone name: " + stone.Name);
            console.log("stone marked: " + this.stoneMarked);
            if (stone.Name === this.stoneMarked) {
                // && "MovementWeatherCode" in stone.Notifications
                // && "Active" in stone.Notifications.MovementWeatherCode
                // && stone.Notifications.MovementWeatherCode.Value === stone.WeatherCode) {
                console.log("Stone has been found");
                this.snackbar.simple("Super, der Stein wurde gefunden. Du bist der Beste!", "blue", "white");
            }
        }
    }
}
